from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(machine: Dict, input_: str, steps: Optional[int] = None) -> Tuple[str, List, bool]:
    history = []
    state = machine['start state']
    tape = list(input_)  # On convertit la chaine du ruban en liste pour faciliter la manipulation
    position = 0
   
    # Limite arbitraire pour éviter les boucles infinies
    #if steps is None:
    #    steps = 1000

    step_counter = 0
    while True:
        if steps is not None and step_counter >= steps: # Fin de boucle si le nombre d'étapes effectué a atteint le nombre prédéfini
            break

        if state in machine['final states']: # Fin de boucle si on arrive à la valeur de "final states"
            break

        reading = tape[position] if position < len(tape) else machine['blank'] # On lit la tape. Si on a dépassé la longueur de la tape initiale (input_), on lit "blank"
        transition = machine['table'][state].get(reading)
        
        if transition is None:  # S'il n'y a pas de transition définie pour l'étape lue sur le moment, on arrête.
            break

        history.append({
            'state': state,
            'reading': reading,
            'position': position,
            'memory': ''.join(tape),
            'transition': transition,
        })

        if isinstance(transition, dict): # Vérifie si la transition courante est un dictionnaire, sinon elle contient juste "L" ou "R"
            tape[position:position+1] = transition.get('write', reading) # Etant un tuple, elle contient une instruction d'écriture
            if 'R' in transition:
                state = transition['R']
                position += 1
            elif 'L' in transition:
                state = transition['L']
                if position == 0:
                    tape.insert(0, machine['blank'])  # Ajoute un nouvel élément à l'extrémité gauche du ruban
                else:
                    position -= 1
        elif transition == 'R':
            position += 1
        elif transition == 'L':
            if position == 0:
                tape.insert(0, machine['blank'])  # Ajoute un nouvel élément à l'extrémité gauche du ruban
            else:
                position -= 1
        step_counter += 1

    __import__('pprint').pprint(history)

    output = ''.join(tape).strip(machine['blank']) # On retire les cases blank de la tape.
    return output, history, state in machine['final states']
